//
//  FNPCartInfoButton.swift
//  Fernsnpetals
//
//  Created by Raj Kumar Sharma on 10/03/17.
//  Copyright © 2017 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPCartInfoButton: UIButton {

    var cartItemInfo: FNPCartItemInfo?
    var indexPath: IndexPath?

}
