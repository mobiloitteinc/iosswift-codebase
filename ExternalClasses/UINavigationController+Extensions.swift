//
//  UINavigationController+Extensions.swift
//  MobileApp
//
//  Created by Raj Kumar Sharma on 10/12/15.
//  Copyright (c) 2015 Mobiloitte. All rights reserved.
//

import Foundation
import UIKit

//lazy var rainbowNavigation = LLRainbowNavigation()
var rainbowNavigation = LLRainbowNavigation()

extension UINavigationController {
    
    func setCustomTransition() {
        rainbowNavigation.wireTo(navigationController: self)
        navigationController?.navigationBar.ll_setBackgroundColor(customNavColor)
        
        navigationController?.navigationBar.ll_setStatusBarMaskColor(kClearColor)
    }
    
    func popWithFadeAnimation() {
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.view.layer.add(transition, forKey: nil)
        self.popViewController(animated: false)
    }
    
    func pushToViewControllerWithFadeAnimation(_ controller:UIViewController) {
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.view.layer.add(transition, forKey: nil)
        self.pushViewController(controller, animated: false)
    }
    
}


//   LLRainbowColorSource
//    // MARK: - ColorSource
//    func ll_navigationBarInColor() -> UIColor {
//        return customNavColor
//    }
//    func ll_navigationBarOutColor() -> UIColor {
//        return UIColor.blueColor()
//    }
