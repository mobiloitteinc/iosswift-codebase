//
//  FNPMakeItSpecialVC.swift
//  Fernsnpetals
//
//  Created by Yasmin Tahira on 11/25/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPMakeItSpecialVC: UIViewController,UITextFieldDelegate ,UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    var pageViewController = UIPageViewController()
    var arrDataSource = NSMutableArray()
    var arrCategoryData = NSMutableArray()
    @IBOutlet weak var viewPageLoad: UIView!
    @IBOutlet weak var viewScrollBase: UIView!
    @IBOutlet weak var scrollViewHeader: UIScrollView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnFilter : UIButton!
    @IBOutlet weak var btnSort : UIButton!
    @IBOutlet var searchBtnAction: UIButton!
    var productIDStr = ""
    var productPrice = ""
    var deliveryDate = ""
    var shippingMethod = ""
    var timeSlotId = ""
    var shippingCost = ""
    var pinCode = ""
    var productType = ""

    var isFromByNow = Bool()
    
    var makeSpecial: FNPMakeSpecial? = nil
    
    
    //Mark : View Life Cycle Management Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.perform(#selector(FNPMakeItSpecialVC.initialisePageController), with: nil, afterDelay: 0.1)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        //calling get data api method
        self.apiMethodToGetData()
    }

    func initialisePageController() {
        self.scrollViewHeader.frame = viewScrollBase.bounds
        self.pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        self.pageViewController.view.frame = self.viewPageLoad.bounds
        let productList = NSMutableArray()
        productList.addObjects(from: ["FLOWERS"])
        productList.addObjects(from: ["CAKES"])
        productList.addObjects(from: ["TEDDIES"])
        productList.addObjects(from: ["CHOCOLATE"])
        productList.addObjects(from: ["CARDS"])
        for index in 0...4 {
            let categoryObj = FNPCategory()
            categoryObj.nameString = productList.object(at: index) as! String
            if index == 0 {
                categoryObj.selectionStatus = true
            }else{
                categoryObj.selectionStatus = false
            }
            arrCategoryData.add(categoryObj)

            let newViewController = FNPMakeItSpecialPageVC(nibName: "FNPMakeItSpecialPageVC", bundle: nil)
            newViewController.tagValue = index + 1000
            arrDataSource.add(newViewController)
        }
        
       // scrollViewHeader.isPagingEnabled = true
        scrollViewHeader.isDirectionalLockEnabled = true
        scrollViewHeader.bounces = false
        self.customizeScrollView()
        let widthWindow = kWindowWidth()
        let widthTemp = CGFloat(widthWindow/3.5)
        let widthScroll = Double(widthTemp)*Double(5.0)
//        let width = 104.0
        scrollViewHeader.contentSize = CGSize(width: Double(widthScroll), height: Double(scrollViewHeader.frame.size.height))
        scrollViewHeader.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        let viewControllers: [UIViewController] = [arrDataSource.object(at: 0) as! UIViewController]
        self.pageViewController.setViewControllers(viewControllers, direction:.forward, animated: false, completion: nil)
        self.addChildViewController(pageViewController)
        self.viewPageLoad.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
    }
    
    func customizeScrollView()  {
        var x_coordinate = Double(0.0)
        let width = kWindowWidth()/3.5
        for index in 0...4 {
            let categoryObj = arrCategoryData.object(at: index) as! FNPCategory
            let viewTemp = UIView.init(frame: CGRect(x: x_coordinate, y: 0.0, width: Double(width), height: Double(scrollViewHeader.frame.size.height)))
            viewTemp.backgroundColor =  UIColor.init(red: 121.0/255.0, green: 188.0/255.0, blue: 223.0/255.0, alpha: 1.0)
            let lblTitle = UILabel.init(frame: viewTemp.bounds)
            lblTitle.backgroundColor = UIColor.clear
            lblTitle.text = categoryObj.nameString
            lblTitle.font = UIFont.init(name: "Roboto-Medium", size: 13.0)
            lblTitle.textAlignment = .center
            if categoryObj.selectionStatus == true {
                lblTitle.textColor = UIColor.init(red: 0.0/255.0, green: 73.0/255.0, blue: 118.0/255.0, alpha: 1.0)
                let viewBase = UIView.init(frame: CGRect(x: 0, y: Double(scrollViewHeader.frame.size.height)-2, width: Double(width), height: 2.0))
                viewBase.backgroundColor = UIColor.init(red: 0.0/255.0, green: 73.0/255.0, blue: 118.0/255.0, alpha: 1.0)
                viewTemp.addSubview(viewBase)
            }else{
                lblTitle.textColor = UIColor.white
                let viewBase = UIView.init(frame: CGRect(x: 0, y: Double(scrollViewHeader.frame.size.height)-2, width: Double(width), height: 2.0))
                viewBase.backgroundColor = UIColor.clear
                viewTemp.addSubview(viewBase)
            }
            
            viewTemp.addSubview(lblTitle)
            let btnTitle = UIButton.init(frame: viewTemp.bounds)
            btnTitle.addTarget(self, action: #selector(FNPMakeItSpecialVC.btnScrollAction(id:)), for: .touchUpInside)
            btnTitle.backgroundColor = UIColor.clear
            btnTitle.tag = index + 1000
            viewTemp.addSubview(btnTitle)
            scrollViewHeader.addSubview(viewTemp)
            x_coordinate = x_coordinate + Double(width)
        }
    }
    
    func btnScrollAction(id: AnyObject) {
        
        let btnIndex = id.tag - 1000
        
        let maxPossibleSpace = Double(self.scrollViewHeader.contentSize.width) - Double(kWindowWidth())
        let requiredOffset = Double(btnIndex) * Double(kWindowWidth()/3.5);
        if requiredOffset > maxPossibleSpace {
            self.scrollViewHeader.setContentOffset(CGPoint.init(x: maxPossibleSpace, y: 0), animated: true)
        }else{
            self.scrollViewHeader.setContentOffset(CGPoint.init(x: requiredOffset, y: 0), animated: true)
        }
        
        
        
        for item in arrCategoryData {
            let categoryObj = item as! FNPCategory
            categoryObj.selectionStatus = false
        }
        let categoryObj = arrCategoryData.object(at: btnIndex) as! FNPCategory
        categoryObj.selectionStatus = true
        self.customizeScrollView()
        let viewControllers: [UIViewController] = [arrDataSource.object(at: btnIndex) as! UIViewController]
        self.pageViewController.setViewControllers(viewControllers, direction:.forward, animated: false, completion: nil)
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let viewControllerIndex = arrDataSource.index(of: viewController)
        if (viewControllerIndex == 0) || (viewControllerIndex == NSNotFound) {
            return nil
        }else{
            let viewControllerTemp = arrDataSource.object(at: viewControllerIndex-1) as!    UIViewController
            return viewControllerTemp
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let viewControllerIndex = arrDataSource.index(of: viewController)
        if (viewControllerIndex == arrDataSource.count-1) || (viewControllerIndex == NSNotFound) {
            return nil
        }else{
            let viewControllerTemp = arrDataSource.object(at: viewControllerIndex+1) as!    UIViewController
            return viewControllerTemp
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]){
        
        let controller = pendingViewControllers.first as! FNPMakeItSpecialPageVC
        var index = Double(0.0)
        index = Double(controller.tagValue - 1000)
        let indexValue = controller.tagValue - 1000
        let maxPossibleSpace = Double(self.scrollViewHeader.contentSize.width) - Double(kWindowWidth())
        let requiredOffset = index * Double(kWindowWidth()/3.5);
        if requiredOffset > maxPossibleSpace {
            self.scrollViewHeader.setContentOffset(CGPoint.init(x: maxPossibleSpace, y: 0), animated: true)
        }else{
            self.scrollViewHeader.setContentOffset(CGPoint.init(x: requiredOffset, y: 0), animated: true)
        }
        for item in arrCategoryData {
            let categoryObj = item as! FNPCategory
            categoryObj.selectionStatus = false
        }
        let categoryObj = arrCategoryData.object(at: indexValue) as! FNPCategory
        categoryObj.selectionStatus = true
        self.customizeScrollView()
    }
    
    //Mark : Memory Management Methods
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBACTION Methods
    @IBAction func backButtonAction(_ sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func menuButtonAction(_ sender: AnyObject) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func noThanksBtnAction(id: AnyObject){
        
        if isFromByNow {
            
            let skipUserValue = UserDefaults.standard.value(forKey: kSkipUser) as? String
            
            if (skipUserValue! as String == "true") {
//                let vc = FNPLoginUserSignInVC(nibName: "FNPLoginUserSignInVC", bundle: nil)
//                vc.isFromExtraSpecial = true
//                self.navigationController!.pushViewController(vc, animated: true)
                
                //call api to add product in cart
                self.webAPIMethodAddToCart()
                
              }else{
                let newViewController = FNPDeliverDetailSavedAddressVC(nibName: "FNPDeliverDetailSavedAddressVC", bundle: nil)
                self.navigationController?.pushViewController(newViewController, animated: true)

            }

        }else{
            
            self.webAPIMethodAddToCart()

//            if let navController = self.navigationController {
//                navController.popViewController(animated: true)
//            }
        }
        
    }
    
    @IBAction func continueBtnAction(id: AnyObject){
        
        if isFromByNow {
            let skipUserValue = UserDefaults.standard.value(forKey: kSkipUser) as? String
            
            if (skipUserValue! as String == "true") {
                let vc = FNPLoginUserSignInVC(nibName: "FNPLoginUserSignInVC", bundle: nil)
                self.navigationController!.pushViewController(vc, animated: true)
            }else{
                let newViewController = FNPDeliverDetailSavedAddressVC(nibName: "FNPDeliverDetailSavedAddressVC", bundle: nil)
                self.navigationController?.pushViewController(newViewController, animated: true)
                
            }
        }else{
            if let navController = self.navigationController {
            navController.popViewController(animated: true)
            }
        }
    }
    
    //MARK:- Web api call methods
    private func apiMethodToGetData() {
        
        let paramDict = NSMutableDictionary()
//        paramDict[KproductId] = self.productIDStr
//        paramDict[KpinCode] = self.pinCode
//        paramDict[KShippingMethod] = self.shippingMethod
//        paramDict[KProductType] = self.productType
//        paramDict[KtimeSlotId] = self.timeSlotId
//        paramDict[KItemIndex] = "0"
//        paramDict[KcatalogId] = "india"
        
                paramDict[KproductId] = "CAKE10508"
                paramDict[KpinCode] = "500095"
                paramDict[KShippingMethod] = "EXPRESS_DELIVERY"
                paramDict[KProductType] = "CAKE"
                paramDict[KtimeSlotId] = "10000"
                paramDict[KItemIndex] = "0"
                paramDict[KcatalogId] = "india"

        
        ServiceHelper.callAPIWithParameters(paramDict, method: .get, apiName: KWeb_API_productAddons, hudType:.smoothProgress) { (result, error) in
            
            if let error = error {
                // show error
                let _ = AlertViewController.alert("Error!", message: error.localizedDescription)
            } else {
                // handle response
                if let dataDict = result as? Dictionary<String, AnyObject> {
                    
                    if ((dataDict[Kstatus] as! Int) == 200){
                        
                       self.makeSpecial = FNPMakeSpecial.init(dictionary: dataDict as NSDictionary)
                    
                        var dataObj : FNPMakeSpecialData? = nil
                        dataObj  = self.makeSpecial?.data
                        
                         var addonDetailsObj : FNPMakeSpecialAddonDetails? = nil
                        addonDetailsObj = dataObj?.addonDetails
                        
                        var addonsDataArray = NSMutableArray()
                        
                        for index in (addonDetailsObj?.addonsList)! {
                            
//                            var addonsListObj : FNPMakeSpecialAddonsList? = nil
//                            addonsListObj  = addonDetailsObj?.addonsList
//                            addonsDataArray.add(addonDetailsObj?.addonsList[index])
                        }
                        
//                        var addonsListObj : FNPMakeSpecialAddonsList? = nil
//                        addonsListObj  = addonDetailsObj?.addonsList
                        
                        
//                        print(">>>%",self.makeSpecial ?? "")
                      
                     
                        //                        self.arrayOccasionType = FNPListData.occasionTypeDataList(dataDict: dataDict)
                        //                        self.webAPIMethodToGetGiftTypeList()
                    } else {
                        //show error
                        let _ = AlertViewController.alert("Error!", message: "something went wrong.")
                    }
                }
            }
        }
    }

    func webAPIMethodAddToCart(){
        
        let paramDict = NSMutableDictionary()
        paramDict[Kproduct_id] = self.productIDStr
        paramDict[Kadd_product_id] = self.productIDStr
        paramDict[Kprice] = self.productPrice
        paramDict[Kquantity] = "1"
        paramDict[KcountryGeoId] = KIND
        paramDict[KfnpSalesChannelEnumId] = KMOBILE_IPHONE
        paramDict[KpinCode] = self.pinCode
        paramDict[KitemDesiredDeliveryDate] = self.deliveryDate
        paramDict[KshippingMethodId] = self.shippingMethod
        paramDict[KtimeSlotId] = self.timeSlotId
        paramDict[KcatalogId] = KcategoryName
        paramDict[KCURRENT_CATALOG_ID] = KcategoryName
        paramDict[KshippingCost] = KshippingCost
        paramDict[Kadd_category_id] = "null"
    
        
        ServiceHelper.callAPIWithParameters(paramDict, method: .post, apiName:KWeb_API_addToCart, hudType:.smoothProgress) { (result, error) in
            
            
            if let error = error {
                
                // show error
                let _ = AlertViewController.alert("Error!", message: error.localizedDescription)
            } else {
                // handle response
                
                if let dataDict = result as? Dictionary<String, AnyObject> {
                    
                    if ((dataDict[Kstatus] as! Int) == 200){
                        
                    } else {
                        let _ = AlertViewController.alert("Error!", message: "something went wrong.")
                    }
                }
            }
        }
    }
    
}
