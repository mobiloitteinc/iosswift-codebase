//
//  FNPMyAccountChangePasswordVC.swift
//  Fernsnpetals
//
//  Created by Priyanka Jaiswal on 12/10/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPMyAccountChangePasswordVC: UIViewController ,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var changePassObj : FNPChangePasswordDetail!
    var isValidRowNumber : NSInteger!
    var isError : Bool!
    
    @IBOutlet var changePasswordTableView: UITableView!
    
    //MARK:- View LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUpMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Initial SetUp Method
    
    private func initialSetUpMethod() {
        
        isValidRowNumber = 1212
        isError = false

        changePassObj = FNPChangePasswordDetail()
        
        //register tableViewCell
        self.changePasswordTableView.register(UINib(nibName:"FNPMyAccountCommonTextFieldTableViewCell", bundle:nil), forCellReuseIdentifier: "FNPMyAccountCommonTextFieldTableViewCell")
    }
    
    //MARK:- UITableView Datasource & Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FNPMyAccountCommonTextFieldTableViewCell", for: indexPath) as! FNPMyAccountCommonTextFieldTableViewCell
        
        //setting tableViewCell selection style
        cell.selectionStyle = .none;
        
        //setting textField delegate
        cell.cellCommonTextField.delegate = self
        
        //setting textField & alertlabel tag
        cell.cellCommonTextField.tag = indexPath.row + 100
        cell.cellCommonErrorLabel.tag = indexPath.row + 10
        
        //setting keyboard attributes
        cell.cellCommonTextField.returnKeyType = UIReturnKeyType.next
        cell.cellCommonTextField.keyboardType = UIKeyboardType.asciiCapable
        
        //handle error label show & hide condition
        if isError == true && isValidRowNumber == indexPath.row {
            cell.cellCommonErrorLabel.text = changePassObj.strCommonError
            cell.cellCommonTextField.lineColor = UIColor.red
        }else{
            cell.cellCommonErrorLabel.text = ""
            cell.cellCommonTextField.lineColor = UIColor.lightGray
        }

        switch indexPath.row {
            
        case 0:
            cell.cellCommonTextField.placeholder = "Email ID"
            cell.cellCommonTextField.text = changePassObj.emailID
            cell.cellCommonTextField.keyboardType = UIKeyboardType.emailAddress
            cell.cellCommonTextField.isSecureTextEntry = false
            break
            
        case 1:
            cell.cellCommonTextField.placeholder = "Mobile Number"
            cell.cellCommonTextField.text = changePassObj.mobileNumber
            cell.cellCommonTextField.isSecureTextEntry = false
            cell.cellCommonTextField.keyboardType = UIKeyboardType.numberPad
            self.addToolBarWithDoneButton(textField: cell.cellCommonTextField)
            break
            
        case 2:
            cell.cellCommonTextField.placeholder = "Old Password"
            cell.cellCommonTextField.text = changePassObj.oldPassword
            break
            
        case 3:
            cell.cellCommonTextField.placeholder = "New Password"
            cell.cellCommonTextField.text = changePassObj.newPassword
            break
            
        case 4:
            cell.cellCommonTextField.placeholder = "Retype Password"
            cell.cellCommonTextField.text = changePassObj.retypePassword
            cell.cellCommonTextField.returnKeyType = UIReturnKeyType.done
            break
            
        default:
            break
        }
        
        return  cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //MARK:- UITextField Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            self.view.viewWithTag(textField.tag+1)?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if isError == true {
            
            isError = false
            self.changePasswordTableView.reloadData()
            
            DispatchQueue.main.async {
                self.view.viewWithTag(textField.tag)?.becomeFirstResponder()
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
                
        switch textField.tag {
            
        case 100:
            changePassObj.emailID = textField.text!
            break
            
        case 101:
            changePassObj.mobileNumber = textField.text!
            break
            
        case 102:
            changePassObj.oldPassword = textField.text!
            break
            
        case 103:
            changePassObj.newPassword = textField.text!
            break
            
        case 104:
            changePassObj.retypePassword = textField.text!
            break
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        
        switch textField.tag {
            
        case 100:
            if resultString.length > 60 {
                return false
            }
            break
            
        case 101:
            if resultString.length > 11{
                return false
            }
            break
            
        case 102:
            if resultString.length > 32 {
            return false
        }
            break

        case 103:
            if resultString.length > 32 {
                return false
            }
            break
            
        case 104:
            if resultString.length > 32{
                return false
            }
            break
            
        default:
            break;
        }
        
        return true;
    }
    
    //MARK:- Validation Method
    
    func areAllFieldsValidated() -> Bool{
        
        var isValidate = false
        
        if trimWhiteSpace(changePassObj.emailID).length == 0 {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please enter email."
        }
            
        else if isValidEmail(trimWhiteSpace(changePassObj.emailID)) == false {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please enter valid email id."
        }
            
        else if trimWhiteSpace(changePassObj.mobileNumber).length == 0 {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please enter mobile number."
        }
        
        else if trimWhiteSpace(changePassObj.mobileNumber).length < 10 {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Mobile number should have atleast 10 digits."
        }
        
        else if isValidMobileNumber(changePassObj.mobileNumber) == false {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please enter valid mobile number."
        }
        
        else if trimWhiteSpace(changePassObj.oldPassword).length == 0{
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please enter old password."
        }
        
        else if trimWhiteSpace(changePassObj.oldPassword).length < 8{
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Password should have atleast 8 characters."
        }
        
        else if trimWhiteSpace(changePassObj.newPassword).length == 0 {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please enter new password."
        }
        
        else if trimWhiteSpace(changePassObj.newPassword).length < 8 {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Password should have atleast 8 characters."
        }
        
        else if trimWhiteSpace(changePassObj.retypePassword).length == 0 {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*Please re-enter password."
            isValidate = false
        }
        
        else if changePassObj.retypePassword != changePassObj.newPassword {
            isError = true
            isValidRowNumber = 0
            changePassObj.strCommonError = "*New password and retype password does not match."
        }else {
            isValidate = true
        }
        
        self.changePasswordTableView.reloadData()
        
        return isValidate
    }
   
    //MARK:- UIButton Action Method
    
    @IBAction func commonBtnActionMethod(_ sender: UIButton) {
        
        self.view.endEditing(true);
        
        switch sender.tag {
            
        case 10,13:
            //Back & Cancel button action
            let _ = navigationController?.popViewController(animated: true)
            break
            
        case 11:
            //Search button action
            let vc = FNPSearchVC(nibName: "FNPSearchVC", bundle: nil)
            kAppDelegate.navController.pushViewController(vc, animated: true)
            break
            
        case 12:
            //Cart button action
            let vc = FNPMyCartVC(nibName: "FNPMyCartVC", bundle: nil)
            kAppDelegate.navController.pushViewController(vc, animated: true)
            break
            
        case 14:
            //Save button action
            if self.areAllFieldsValidated() {
                self.callApiChangePassword()
            }
            break
            
        default:
            break;
        }
    }
    
    //MARK:- API Request & Response Method
    
    func callApiChangePassword() {
        
        
    }
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

