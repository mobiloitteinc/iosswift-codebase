//
//  AppDelegate.swift
//  Fernsnpetals
//
//  Created by Suresh patel on 22/11/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Google
import Fabric
import Crashlytics

//Font.kRobotoBoldWithSize(size: 14)


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var isReachable = false
    
    var navController = UINavigationController()
    
    var slideMenuController: ExSlideMenuController?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        //remember me code
        if UserDefaults.standard.object(forKey: KCustomerId) != nil{
            kAppDelegate.addSlidePannelController()
        }
        else {
            let landingVC = FNPLandingVC(nibName: "FNPLandingVC", bundle: nil)
            let nvc: UINavigationController = UINavigationController(rootViewController: landingVC)
            nvc.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = nvc
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.statusBarView?.backgroundColor = RGBA(29, g: 94, b: 46, a: 1.0)
        self.setupReachability()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Fabric.with([Crashlytics.self])
        
        
        // get user agent of iOS
        let webView = UIWebView(frame: CGRect.zero)
        let secretAgent: String? = webView.stringByEvaluatingJavaScript(from: "navigator.userAgent")
        UserDefaults.standard.setValue("\(secretAgent!)", forKey: "userAgent")
        UserDefaults.standard.synchronize()
        
        return true
    }
    
    func logOut(){
       // FacebookManager.facebookManager.logoutFromFacebook()
        GIDSignIn.sharedInstance().signOut()
        FNPAppUtility.deleteCustomerId()
        
        let landingVC = FNPLandingVC(nibName: "FNPLandingVC", bundle: nil)
        let nvc: UINavigationController = UINavigationController(rootViewController: landingVC)
        self.window?.rootViewController = nvc
    }
    
    fileprivate func setupReachability() {
        
        // Allocate a reachability object
        let reach = Reachability.forInternetConnection()
        self.isReachable = (reach?.isReachable())!
        
        // Set the blocks
        
        reach?.reachableBlock = { (reachability) in
            
            DispatchQueue.main.async(execute: {
                self.isReachable = true
            })
        }
        
        reach?.unreachableBlock = { (reachability) in
            
            DispatchQueue.main.async(execute: {
                self.isReachable = false
            })
        }
        reach?.startNotifier()
    }
    
    func addSlidePannelController(){
        
        let homeVC = FNPHomeVC(nibName: "FNPHomeVC", bundle: nil)
        let leftMenuVC = FNPSlideMenuVC(nibName: "FNPSlideMenuVC", bundle: nil)
        let navigationMain: UINavigationController = UINavigationController(rootViewController: homeVC)
        navigationMain.setNavigationBarHidden(true, animated: false)
        
        let navigationLeft: UINavigationController = UINavigationController(rootViewController: leftMenuVC)
        leftMenuVC.mainViewController = navigationMain
        navigationLeft.setNavigationBarHidden(true, animated: false)
        
        let slideMenuController = ExSlideMenuController(mainViewController:navigationMain, leftMenuViewController: navigationLeft)
        
        self.slideMenuController = slideMenuController
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = false
        slideMenuController.leftPanGesture?.isEnabled = true
        slideMenuController.rightPanGesture?.isEnabled = true
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        navController = UINavigationController.init(rootViewController: slideMenuController)
        navController.setNavigationBarHidden(true, animated: false)
        self.window?.rootViewController = navController
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if #available(iOS 9.0, *) {
            
            let facebookHandled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            let googleHandled = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            if facebookHandled == true {
                return facebookHandled
            }
            return googleHandled
            
        } else {
            return false
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let facebookHandled = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        let googleHandled = GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        if facebookHandled == true {
            return facebookHandled
        }
        return googleHandled
    }
}

