//
//  ServiceHelper.swift
//  Template
//
//  Created by Raj Kumar Sharma on 05/04/16.
//  Copyright © 2016 Mobiloitte. All rights reserved.
//

import UIKit
import MobileCoreServices
//@@@@@@  Development URL
//@@@@@@  "https://api-cache.fnp.com/productapi/api/rest/v1.0/"
//@@@@@@  Production URL
//@@@@@@  "https://api-live.fnp.com/productapi/api/rest/v1.0/"

let development = false

// Production URL

let baseURL: String = development == true ? "https://api-cache.fnp.com/api/rest/v1.0/" : "https://api-cache.fnp.com/api/rest/v1.0/"

let timeoutInterval:Double = 60

enum loadingIndicatorType: CGFloat {
    
    case `default`  = 0 // showing indicator & text by disable UI
    case simple  = 1 // // showing indicator only by disable UI
    case noProgress  = 2 // without indicator by disable UI
    case smoothProgress  = 3 // without indicator by enable UI i.e No hud
}

enum MethodType: CGFloat {
    case get  = 0
    case post = 1
    case put  = 2
    case delete  = 3
}

//var hud_type: loadingIndicatorType = .Default

class ServiceHelper: NSObject {
    
    //MARK:- Public Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    public class func getBaseUrl() -> String
    {
        return baseURL;
    }
    class func callAPIWithParameters(_ parameterDict:NSMutableDictionary, method:MethodType, apiName :String, hudType:loadingIndicatorType, completionBlock: @escaping (AnyObject?, NSError?) -> Void) ->Void {
        //hud_type = hudType
        if (kAppDelegate.isReachable == false) {
            let _ = AlertViewController.alert("Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            
            return
        }
        
        //>>>>>>>>>>> create request
        let url = requestURL(method, apiName: apiName, parameterDict: parameterDict)
        var request = URLRequest(url: url)
        request.httpMethod = methodName(method)
        
        //>>>>>>>>>>>> insert json data to the request
        
        if (apiName == "login") {
            request.setValue((parameterDict["password"] as! NSString) as String, forHTTPHeaderField: "login.password")
            request.setValue((parameterDict["username"] as! NSString) as String, forHTTPHeaderField: "login.username")
            request.setValue((parameterDict["logintoken"] as! NSString) as String, forHTTPHeaderField: "login.token")
        }
        let sessionToken: String = FNPAppUtility.getSessionTokenFromDefault()
        if (sessionToken.length>0)
        {
            request.setValue("JSESSIONID=\(sessionToken)", forHTTPHeaderField: "Cookie")
        }
        
        let userAgent : String =  UserDefaults.standard.value(forKey: "userAgent") as! String
        print(userAgent)
        if userAgent.length > 0 {
            request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        }
        
        if (apiName == KWeb_API_sendOTP || apiName == KWeb_API_validateOTP) {
            request.httpBody = formBodyForOtp(parameterDict: parameterDict)
            
        }else if (apiName == "signUp" || apiName == "loginOrSignupGoogleUser" || apiName == "loginOrSignupFacebookUser" || apiName == KWeb_API_addToCart || apiName == KWeb_API_productAddons || apiName == KWeb_API_addToCart || apiName == kWeb_API_addAddonToCart || apiName == KWeb_API_validateOTP || apiName == kWeb_API_UpdateCartGetNext || apiName == kWeb_API_createOrder || apiName == kWeb_Api_storePaypalResponce || apiName == kWeb_Api_addEditReminder || apiName == kWeb_API_getPayuHashDetails || apiName == kWeb_API_logout) {
            
            request.httpBody = formBody(parameterDict: parameterDict)
        }else {
            request.httpBody = body(method, parameterDict: parameterDict)
        }
        
        
        request.timeoutInterval = timeoutInterval
        if method == .post  || method == .put {
            if (apiName == "signUp" || apiName == "forgotPassword" || apiName == "loginOrSignupGoogleUser" || apiName == "loginOrSignupFacebookUser" || apiName == KWeb_API_addToCart || apiName == kWeb_API_addAddonToCart || apiName == KWeb_API_sendOTP || apiName == KWeb_API_validateOTP || apiName == kWeb_API_UpdateCartGetNext || apiName == kWeb_API_createOrder || apiName == kWeb_Api_storePaypalResponce || apiName == kWeb_Api_addEditReminder || apiName == kWeb_API_getPayuHashDetails || apiName == kWeb_API_logout) {
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            }else {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            
        }
        
        hideAllHuds(false, type:hudType)
        executeRequest(rqst: request , hudType: hudType , completionBlock: completionBlock)
    }
    
    
    class func callAPIToGetWithURL(_ urlString:NSString, method:MethodType, apiName :String, hudType:loadingIndicatorType, completionBlock: @escaping (AnyObject?, NSError?) -> Void) ->Void {
        //hud_type = hudType
        if (kAppDelegate.isReachable == false) {
            let _ = AlertViewController.alert("Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            
            return
        }
        
        //>>>>>>>>>>> create request
        let url = requestURLGET(method, apiName: apiName, urlString: urlString)
        var request = URLRequest(url: url)
        request.httpMethod = methodName(method)
        request.timeoutInterval = timeoutInterval
        let sessionToken: String = FNPAppUtility.getSessionTokenFromDefault()
        if (sessionToken.length>0)
        {
            request.setValue("JSESSIONID=\(sessionToken)", forHTTPHeaderField: "Cookie")
        }

        hideAllHuds(false, type:hudType)
               // Load configuration into Session
        //logInfo("\n\n Request URL  >>>>>>\(url)")
        
        executeRequest(rqst: request , hudType: hudType , completionBlock: completionBlock)
    }
    
    
  class func executeRequest(rqst:URLRequest,hudType:loadingIndicatorType, completionBlock: @escaping (AnyObject?, NSError?) -> Void) -> Void
    {
        
        let config = URLSessionConfiguration.default
        // Session Configuration
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: rqst, completionHandler: {
            (data, response, error) in
            
             hideAllHuds(true, type:hudType)
            
            if error != nil {
                print("\n\n\n\n************************")
                print("************************\n")
                print("Request URL= \(String(describing: rqst.url))\n")
                print("Request Headers= \(String(describing: rqst.allHTTPHeaderFields?.description))\n")
                if (rqst.httpBody != nil)
                {
                    if let returnData = String(data: rqst.httpBody!, encoding: .utf8)
                    {
                        print("Request Body= \(returnData)\n")
                        
                    }
                }
                print("************************")
                print("\n************************\n\n\n\n")
                print("Response Error=\(String(describing: error))")
                
                // logInfo("\n\n error  >>>>>>\n\(error)")
                DispatchQueue.main.async(execute: {
                    completionBlock(nil,error as NSError?)
                })
                
            } else {
                
                let httpResponse = response as! HTTPURLResponse
                let responseCode = httpResponse.statusCode
                let responseString = NSString.init(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                print("\n\n\n\n************************")
                print("************************\n")
                print("Request URL= \(String(describing: rqst.url))\n")
                print("Request Headers= \(String(describing: rqst.allHTTPHeaderFields?.description))\n")
                if (rqst.httpBody != nil)
                {
                    if let returnData = String(data: rqst.httpBody!, encoding: .utf8)
                    {
                        print("Request Body= \(returnData)\n")
                        
                    }
                }
                print("************************")
                print("\n************************\n\n\n\n")
                print("Response Body=\(String(describing: responseString))")
 
                    do
                    {
                        
                        let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        
                        if let dataDict = result as? Dictionary<String, AnyObject>
                        {
                            var statusCode = 0
                            
                            if (dataDict[Kstatus] != nil)
                            {
                                if let strStatus = dataDict[Kstatus] as? Int {
                                    statusCode = strStatus
                                }else if let strStatus =  dataDict[Kstatus] as? String{
                                    statusCode = Int(strStatus)!
                                }
                            }
                            else if ((dataDict["statusCode"] as! String) == "500")
                            {
                              statusCode = Int(dataDict["statusCode"] as! String)!
                            }
                             if (statusCode == 200)
                             {
                                 DispatchQueue.main.async(execute: {
                                    completionBlock(result as AnyObject?,nil)
                                 })
                              }
                            else if (statusCode == 403)
                              {
                                let secondRequest = rqst
                                var newReques = URLRequest.init(url: URL.init(string: baseURL+"loginByAccessToken")!)
                                newReques.httpMethod = "POST"
                                newReques.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                                let paramDi = NSMutableDictionary()
                                paramDi["userLoginId"] = UserDefaults.standard.value(forKey: "email")
                                paramDi["fnpAccessToken"] = UserDefaults.standard.value(forKey: kfnpAccessToken)
                                newReques.httpBody = formBody(parameterDict: paramDi)
                                let sessionToken: String = FNPAppUtility.getSessionTokenFromDefault()
                                if (sessionToken.length>0)
                                {
                                    newReques.setValue("JSESSIONID=\(sessionToken)", forHTTPHeaderField: "Cookie")
                                }
                                
                                let serviceHandler = FNPServiceHandler()
                                serviceHandler.execTask(request: newReques, secondRqst: secondRequest, taskCallback: { (authResponse, authData, secondReqst) in
                                    
                                    _ = authResponse as! HTTPURLResponse
                                   // let resCode = httpResp.statusCode
                                    let secondResponseString = NSString.init(data: authData, encoding: String.Encoding.utf8.rawValue)
                                    print("\n\n\n\n************************")
                                    print("************************\n")
                                    print("Request URL= \(String(describing: secondReqst.url))\n")
                                    print("Request Headers= \(String(describing: secondReqst.allHTTPHeaderFields?.description))\n")
                                    //print("Request Body= \(parameterDict.toJsonString())\n")
                                    print("************************")
                                    print("\n************************\n\n\n\n")
                                    print("Response Body=\(String(describing: secondResponseString))")
                                    
                                    do
                                    {
                                        
                                        let secondResult = try JSONSerialization.jsonObject(with: authData, options: JSONSerialization.ReadingOptions.mutableContainers)
                                        if let dataDict = secondResult as? Dictionary<String, AnyObject>
                                        {
                                            
                                           if (dataDict[Kstatus] != nil)
                                           {
                                               if ((dataDict[Kstatus] as! Int) == 200)
                                               {
                                                let sessinID = dataDict["JSESSIONID"] as! String
                                                let dataDic = dataDict["data"] as! NSDictionary
                                                
                                                
                                                if dataDic["fnpAccessToken"] != nil {
                                                    
                                                    let fnpAccessToken = dataDic["fnpAccessToken"] as! String
                                                    FNPAppUtility.saveFnpTokenInDefault(fnpTokenId: fnpAccessToken)
                                                    FNPAppUtility.saveSessionTokenInDefault(sessionToken: sessinID)
                                                }
                                                
                                               executeRequest(rqst: secondRequest , hudType: hudType , completionBlock: completionBlock)
                                               }
                                            }
                                            else
                                            {
                                                let result = ["status":500]
                                                DispatchQueue.main.async(execute: {
                                                    completionBlock(result as AnyObject?,nil)
                                                })
                                            }
                                        }
                                        else
                                        {
                                            let result = ["status":500]
                                            DispatchQueue.main.async(execute: {
                                                completionBlock(result as AnyObject?,nil)
                                            })
                                        }
                                    
                                    }
                                    catch
                                    {
                                        let result = ["status":500]
                                        DispatchQueue.main.async(execute: {
                                            completionBlock(result as AnyObject?,nil)
                                        })
                                     }
                                   
                                })
 
                              }
                            else
                             {
                            
                              DispatchQueue.main.async(execute: {
                                completionBlock(result as AnyObject?,nil)
                               })
                              }
                         }
                       else
                        {
                            let result = ["status":500]
                            DispatchQueue.main.async(execute: {
                                completionBlock(result as AnyObject?,nil)
                            })

                        }
                    }
                    catch {
                        
                        // logInfo("\n\n error in JSONSerialization")
                        logInfo("\n\n error  >>>>>>\n\(error)")
                        
                        if responseCode == 200 {
                            let result = ["status":500]
                            DispatchQueue.main.async(execute: {
                                completionBlock(result as AnyObject?,nil)
                            })
                        }
                        
                    }
                    
                }
 
        })
        task.resume()
    }
    
    
    //MARK:- Private Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    class fileprivate func methodName(_ method:MethodType)-> String {
        
        switch method {
        case .get: return "GET"
        case .post: return "POST"
        case .delete: return "DELETE"
        case .put: return "PUT"
        }
    }
    
    class fileprivate func body(_ method:MethodType, parameterDict:NSMutableDictionary) -> Data {
        
        switch method {
        case .get: return Data()
        case .post: return parameterDict.toNSData()
        case .put: return parameterDict.toNSData()
            
        default: return Data()
        }
    }
    
    class fileprivate func formBody(parameterDict:NSMutableDictionary) -> Data {
        var body = ""
        var isFirst = true
        
        for key in parameterDict.allKeys {
            
            let object = parameterDict[key as! String]
            
            if object is NSArray {
                
                let array = object as! NSArray
                for eachObject in array {
                    var appendedStr = "&"
                    if (isFirst == true) {
                        appendedStr = "?"
                    }
                    body += appendedStr + (key as! String) + "=" + (eachObject as! String)
                    isFirst = false
                }
                
            } else {
                let appendedStr = "&"
                let parameterStr = parameterDict[key as! String] as! String
                body += appendedStr + (key as! String) + "=" + parameterStr
            }
            
            isFirst = false
        }
        
        let strData = body.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        
        
        print(body);
        let data = strData?.data(using: .utf8)!
        
        return data!
    }
    
    
    class fileprivate func formBodyForOtp(parameterDict:NSMutableDictionary) -> Data {
        
        
        var body = ""
        var isFirst = true
        
        for key in parameterDict.allKeys {
            
            let object = parameterDict[key as! String]
            
            if object is NSArray {
                
                let array = object as! NSArray
                for eachObject in array {
                    var appendedStr = "&"
                    if (isFirst == true) {
                        appendedStr = "?"
                    }
                    body += appendedStr + (key as! String) + "=" + (eachObject as! String)
                    isFirst = false
                }
                
            } else {
                let appendedStr = "&"
                //                if (isFirst == true) {
                //                    appendedStr = "?"
                //                }
                let parameterStr = parameterDict[key as! String] as! String
                body += appendedStr + (key as! String) + "=" + parameterStr
            }
            
            isFirst = false
        }
        let data = body.data(using: .utf8)!
        return data
    }
    
    class fileprivate func requestURL(_ method:MethodType, apiName:String, parameterDict:NSMutableDictionary) -> URL {
        let urlString = baseURL + apiName
        
        switch method {
        case .get:
            return getURL(apiName, parameterDict: parameterDict)
            
        case .post:
            return URL(string: urlString)!
            
        case .put:
            return URL(string: urlString)!
            
        default: return URL(string: urlString)!
        }
    }
    
    class fileprivate func requestURLGET(_ method:MethodType, apiName:String, urlString:NSString) -> URL {
        
        let urlString = urlString as String
        return URL(string: urlString)!
    }
    
    
    class fileprivate func getURL(_ apiName:String, parameterDict:NSMutableDictionary) -> URL {
        
        var urlString = baseURL + apiName
        var isFirst = true
        
        for key in parameterDict.allKeys {
            
            let object = parameterDict[key as! String]
            
            if object is NSArray {
                
                let array = object as! NSArray
                for eachObject in array {
                    var appendedStr = "&"
                    if (isFirst == true) {
                        appendedStr = "?"
                    }
                    urlString += appendedStr + (key as! String) + "=" + (eachObject as! String)
                    isFirst = false
                }
                
            } else {
                var appendedStr = "&"
                if (isFirst == true) {
                    appendedStr = "?"
                }
                let parameterStr = parameterDict[key as! String] as! String
                urlString += appendedStr + (key as! String) + "=" + parameterStr
            }
            
            isFirst = false
        }
        
        let strUrl = urlString.addingPercentEscapes(using: String.Encoding.utf8)
        
        return URL(string:strUrl!)!
    }
    
    
    
    // Create request
    ///
    /// - parameter userid:   The userid to be passed to web service
    /// - parameter password: The password to be passed to web service
    /// - parameter email:    The email address to be passed to web service
    ///
    /// - returns:            The NSURLRequest that was created
    
    
    class func uploadImageFromRequest(currentRequest : URLRequest ,hudType:loadingIndicatorType, completionBlock: @escaping (AnyObject?, NSError?) -> Void) -> Void
    {
        
         hideAllHuds(false, type:hudType)
        let config = URLSessionConfiguration.default
        // Session Configuration
        let session = URLSession(configuration: config)
        // Load configuration into Session
        //logInfo("\n\n Request URL  >>>>>>\(url)")
        let task = session.dataTask(with: currentRequest, completionHandler: {
            (data, response, error) in
            
            hideAllHuds(true, type:hudType)
            if error != nil {
                print("\n\n\n\n************************")
                print("************************\n")
                print("Request URL= \(String(describing: currentRequest.url))\n")
                print("Request Headers= \(String(describing: currentRequest.allHTTPHeaderFields?.description))\n")
                //  print("Request Body= \(parameterDict.toJsonString())\n")
                print("************************")
                print("Response Error=\(String(describing: error))")
                print("\n************************\n\n\n\n")
                logInfo("\n\n error  >>>>>>\n\(String(describing: error))")
                DispatchQueue.main.async(execute: {
                completionBlock(nil,error as NSError?)
                 })
                
            } else {
                
                let responseString = NSString.init(data: data!, encoding: String.Encoding.utf8.rawValue)
                // logInfo("\nResponse String>>>> \n \(responseString)")
                print("\n\n\n\n************************")
                print("New Web Service")
                print("************************\n")
                print("Request URL= \(String(describing: currentRequest.url))\n")
                print("Request Headers= \(String(describing: currentRequest.allHTTPHeaderFields?.description))\n")
                //print("Request Body= \(parameterDict.toJsonString())\n")
                print("************************")
                print("Response Body=\(String(describing: responseString))")
                print("\n************************\n\n\n\n")
            }
            
            do {
               
               if (data != nil)
               {
                let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                
                if let dataDict = result as? Dictionary<String, AnyObject>
                {
                    if (dataDict[Kstatus] != nil)
                    {
                        if ((dataDict[Kstatus] as! Int) == 200)
                        {
                            DispatchQueue.main.async(execute: {
                                completionBlock(result as AnyObject?,nil)
                            })
                        }
                        else
                        {
                            DispatchQueue.main.async(execute: {
                                completionBlock(result as AnyObject?,nil)
                            })
                        }
                    }
                    else
                    {
                        let newResult = (result as! NSMutableDictionary)
                        newResult.setValue(500, forKey: "status")
                        DispatchQueue.main.async(execute: {
                            completionBlock(result as AnyObject?,nil)
                        })
                    }
                    
                }
                else
                {
                    let result = ["status":500]
                    DispatchQueue.main.async(execute: {
                        completionBlock(result as AnyObject?,nil)
                    })
                }
                
                }

                }
            catch
            {
                let result = ["status":500]
                DispatchQueue.main.async(execute: {
                    completionBlock(result as AnyObject?,nil)
                })
            }
        })
        task.resume()
        
    }
    
    
    class  func createRequest(urlString: String, imgData : Data)  -> URLRequest {
        //        let parameters = [
        //            "user_id"  : userid,
        //            "email"    : email,
        //            "password" : password]  // build your dictionary however appropriate
        
        let boundary = ServiceHelper.generateBoundaryString()
        
        let url = URL(string: baseURL + urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let sessionToken: String = FNPAppUtility.getSessionTokenFromDefault()
        if (sessionToken.length>0)
        {
            request.setValue("JSESSIONID=\(sessionToken)", forHTTPHeaderField: "Cookie")
        }
        //let path1 = Bundle.main.path(forResource: "image1", ofType: "png")!
        request.httpBody = try! ServiceHelper.createBody(with: nil, filePathKey: "uploaded_file", paths: [], imgData: imgData, boundary: boundary)
        // request.httpBody = try ServiceHelper.createBody(with: nil, filePathKey: "file", paths: imgPaths, boundary: boundary)
        if request.httpBody != nil
        {
            return request
        }
        else
        {
            return request
        }
        
    }
    
    /// Create body of the multipart/form-data request
    ///
    /// - parameter parameters:   The optional dictionary containing keys and values to be passed to web service
    /// - parameter filePathKey:  The optional field name to be used when uploading files. If you supply paths, you must supply filePathKey, too.
    /// - parameter paths:        The optional array of file paths of the files to be uploaded
    /// - parameter boundary:     The multipart/form-data boundary
    ///
    /// - returns:                The NSData of the body of the request
    
    class fileprivate func  createBody(with parameters: [String: String]?, filePathKey: String, paths: [String],imgData :Data, boundary: String) throws -> Data {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        //        for path in paths {
        //            let url = URL(fileURLWithPath: path)
        //            let filename = url.lastPathComponent
        //            let data = try Data(contentsOf: url)
        //            //let mimetype = mimeType(for: path)
        //            let mimetype = "image/png"
        //            body.append("--\(boundary)\r\n")
        //            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
        //            body.append("Content-Type: \(mimetype)\r\n\r\n")
        //            body.append(data)
        //            body.append("\r\n")
        //        }
        //    let url = URL(fileURLWithPath: path)
        //    let filename = url.lastPathComponent
        //    let data = try Data(contentsOf: url)
        //let mimetype = mimeType(for: path)
        
        
        let mimetype = "image/png"
        let filename = "image.png"
        body.append("--\(boundary)\r\n")
        body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
        body.append("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imgData)
        body.append("\r\n")
        
        body.append("--\(boundary)--\r\n")
        return body
    }
    
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    class fileprivate func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    /// Determine mime type on the basis of extension of a file.
    ///
    /// This requires MobileCoreServices framework.
    ///
    /// - parameter path:         The path of the file for which we are going to determine the mime type.
    ///
    /// - returns:                Returns the mime type if successful. Returns application/octet-stream if unable to determine mime type.
    
    class fileprivate func mimeType(for path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream";
    }
    
    
    
    
    
}

private func hideAllHuds(_ status:Bool, type:loadingIndicatorType) {
    //UIApplication.sharedApplication().networkActivityIndicatorVisible = !status
    
    if (type == .smoothProgress) {
        return
    }
    
    DispatchQueue.main.async(execute: {
        var hud = MBProgressHUD(for: kAppDelegate.window!)
        if hud == nil {
            hud = MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
        }
        hud?.cornerRadius = 8.0
        hud?.bezelView.color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        hud?.margin = 12
        hud?.activityIndicatorColor = UIColor.white
        
        if (status == false) {
            if (type  == .noProgress) {
                
            } else {
                hud?.show(animated: true)
            }
        } else {
            hud?.hide(animated: true, afterDelay: 0.3)
        }
    })
}

private func isJWTokenRequired(_ apiName:String) -> Bool {
    
    if apiName == kAPINameLogin {
        return true
    }
    
    return false
}

extension NSDictionary {
    func toNSData() -> Data {
        return try! JSONSerialization.data(withJSONObject: self, options: [])
    }
    
    func toJsonString() -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        return jsonString
    }
}

extension Data {
    
    /// Append string to NSMutableData
    ///
    /// Rather than littering my code with calls to `dataUsingEncoding` to convert strings to NSData, and then add that data to the NSMutableData, this wraps it in a nice convenient little extension to NSMutableData. This converts using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `NSMutableData`.
    
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
