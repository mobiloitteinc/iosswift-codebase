//
//  FNPMyAccountRemainderCell.swift
//  Fernsnpetals
//
//  Created by Priyanka Jaiswal on 12/8/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPMyAccountRemainderCell: UITableViewCell {

    @IBOutlet var pickerButton: UIButton!
    @IBOutlet var reminderTextField: UITextField!
    @IBOutlet var pickerImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
