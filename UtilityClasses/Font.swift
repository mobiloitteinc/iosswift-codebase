//
//  Font.swift
//  HearYourNews
//
//  Created by Raj Kumar Sharma on 04/04/16.
//  Copyright © 2016 Mobiloitte. All rights reserved.
//

import UIKit

class Font: NSObject {
    
        //MARK:- Roboto Font With Size >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    class func robotoRegular(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Regular", size: size)!
    }
    
    class func robotoThin(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Thin", size: size)!
    }
    
    class func robotoThinItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-ThinItalic", size: size)!
    }
    
    class func robotoMedium(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Medium", size: size)!
    }
    
    class func robotoMediumItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-MediumItalic", size: size)!
    }
    
    class func robotoItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Italic", size: size)!
    }
    
    class func robotoLight(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Light", size: size)!
    }
    
    class func robotoLightItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-LightItalic", size: size)!
    }
    
    class func robotoBold(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Bold", size: size)!
    }
    
    class func robotoBoldItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-BoldItalic", size: size)!
    }
    
    class func robotoBlack(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-Black", size: size)!
    }
    
    class func robotoBlackItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"Roboto-BlackItalic", size: size)!
    }
    
    //MARK:- RobotoCondensed Font With Size >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    class func robotoCondensedRegular(size: CGFloat) -> UIFont {
        return UIFont(name:"RobotoCondensed-Regular", size: size)!
    }
    
    class func robotoCondensedBold(size: CGFloat) -> UIFont {
        return UIFont(name:"RobotoCondensed-Bold", size: size)!
    }
    
    class func robotoCondensedBoldItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"RobotoCondensed-BoldItalic", size: size)!
    }
    
    class func robotoCondensedLight(size: CGFloat) -> UIFont {
        return UIFont(name:"RobotoCondensed-Light", size: size)!
    }
    
    class func robotoCondensedLightItalic(size: CGFloat) -> UIFont {
        return UIFont(name:"RobotoCondensed-LightItalic", size: size)!
    }
    
    
}
