//
//  FNPProductDetailInfo.swift
//  Fernsnpetals
//
//  Created by Mirza Zuhaib Beg on 11/25/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPProductDetailInfo: NSObject {

    var isDescriptionOpen : Bool = false
    var isDeliveryInfoOpen : Bool = false
    var isCaseInstructionOpen : Bool = false
    var careInstructionsContent = NSString()
    var defaultPrice = NSString()
    var deliveryInfoContent = NSString()
    var featureDesc = NSString()
    var isSubVariantExist = NSString()
    var largeImageUrl = NSString()
    var largeImgsCount = NSString()
    var listPrice = NSString()
    var longDescription = NSString()
    var price = NSString()
    var primaryProductCategoryId = NSString()
    var productId = NSString()
    var productName = NSString()
    var productTypeId = NSString()
    var skuCode = NSString()
    var smallImageUrl = NSString()
    
    
    class func getProductDetail(dataDict: [String : AnyObject]) -> FNPProductDetailInfo {
        
        let productDetailInfo = FNPProductDetailInfo()
        
        productDetailInfo.careInstructionsContent = "\(dataDict.validatedValue(KcareInstructionsContent, expected: "" as AnyObject))" as NSString
        productDetailInfo.defaultPrice = "\(dataDict.validatedValue(KdefaultPrice, expected: "" as AnyObject))" as NSString
        productDetailInfo.deliveryInfoContent = "\(dataDict.validatedValue(KdeliveryInfoContent, expected: "" as AnyObject))" as NSString
        productDetailInfo.featureDesc =  "\(dataDict.validatedValue(KfeatureDesc, expected: "" as AnyObject))" as NSString
        productDetailInfo.largeImageUrl =  "\(dataDict.validatedValue(KlargeImageUrl, expected: "" as AnyObject))" as NSString
        productDetailInfo.largeImgsCount = "\(dataDict.validatedValue(KlargeImgsCount, expected: "" as AnyObject))" as NSString
        productDetailInfo.listPrice =  "\(dataDict.validatedValue(KlistPrice, expected: "" as AnyObject))" as NSString
        productDetailInfo.price =  "\(dataDict.validatedValue(Kprice, expected: "" as AnyObject))" as NSString
        productDetailInfo.longDescription =  "\(dataDict.validatedValue(KlongDescription, expected: "" as AnyObject))" as NSString
        productDetailInfo.price = "\(dataDict.validatedValue(Kprice, expected: "" as AnyObject))" as NSString
        productDetailInfo.primaryProductCategoryId =  "\(dataDict.validatedValue(KprimaryProductCategoryId, expected: "" as AnyObject))" as NSString
        productDetailInfo.productId =  "\(dataDict.validatedValue(KproductId, expected: "" as AnyObject))" as NSString
        productDetailInfo.productName =  "\(dataDict.validatedValue(KproductName, expected: "" as AnyObject))" as NSString
        productDetailInfo.skuCode =  "\(dataDict.validatedValue(KskuCode, expected: "" as AnyObject))" as NSString
        productDetailInfo.smallImageUrl =  "\(dataDict.validatedValue(KsmallImageUrl, expected: "" as AnyObject))" as NSString

        return productDetailInfo
    }
    
}




