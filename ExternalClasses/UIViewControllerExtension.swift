//
//  UIViewControllerExtension.swift
//  MobileApp
//
//  Created by Raj Kumar Sharma on 26/10/15.
//  Copyright © 2015 Mobiloitte. All rights reserved.
//

import UIKit
import CoreLocation

public extension UIViewController {
    
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
        self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func setLeftNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.addLeftGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }

    public func moveUIComponentWithValue(_ value:CGFloat, forLayoutConstraint:NSLayoutConstraint, forDuration:TimeInterval) {
        UIView.beginAnimations("MoveView", context: nil)
        UIView.setAnimationCurve(.easeInOut)
        UIView.setAnimationDuration(forDuration)
        forLayoutConstraint.constant = value
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }
    
    public func animateUIComponentWithValue(_ value:CGFloat, forLayoutConstraint:NSLayoutConstraint, forDuration:TimeInterval) {
        
        forLayoutConstraint.constant = value
        
        UIView.animate(withDuration: forDuration, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
            
            }) { (Bool) -> Void in
                // do anything on completion
        }
    }
    
    func checkIfLocationServicesEnabled() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
                
            case .denied:
                
                let _ = AlertViewController.alert("App Permission Denied", message: "To re-enable, please go to Settings and turn on Location Service for this app. We will be using the \"Financial District\" as your default location.", buttons: ["YES", "NO"], tapBlock: { (alertAction, index) in
                    if index == 0 {
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }
                })
                
                break
            case .notDetermined, .restricted:
                logInfo("No access")
                let _ = AlertViewController.alert("", message: "Unable to update location, Please enable Location Services from your smartphone's settings menu. We will be using the \"Financial District\" as your default location.", buttons: ["YES", "NO"], tapBlock: { (alertAction, index) in
                    if index == 0 {
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }
                })
                break
            case .authorizedAlways, .authorizedWhenInUse:
                logInfo("Access")
            }
        } else {
            
            let _ = AlertViewController.alert("", message: "Unable to update location, Please enable Location Services from your smartphone's settings menu. We will be using the \"Financial District\" as your default location.", buttons: ["YES", "NO"], tapBlock: { (alertAction, index) in
                if index == 0 {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }
            })
        }
    }
    
    func addToolBarWithDoneButton(textField:UITextField) {
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.barStyle = .default
        
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done,
                                            target: view, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }

}
