//
//  FNPDeliveryDetailsInfo.swift
//  Fernsnpetals
//
//  Created by Yogita Joshi on 07/03/17.
//  Copyright © 2017 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPDeliveryDetailsInfo: NSObject {
    
    var strCakeMsg = ""
    var strCardMsg = ""
    var strCartItemIndex = ""
    var strCity = ""
    var strCountryGeoId = ""
    var strCountryName = ""
    var strDeliveryDate = ""
    var strEndTimeSlot = ""
    var strItemSubTotal = ""
    var strOccassion = ""
    var strPinCode = ""
    var postalTelecomNumber = ""
    var productAddOnList = NSMutableArray()
    var strProductId = ""
    var strProductImage = ""
    var strProductName = ""
    var strProductPrimaryCategoryId = ""
    var strQuantity = ""
    var strRecipientNameCard = ""
    var relevantPostalAddressList = ""
    var strSenderName = ""
    var strShippingMethodName = ""
    var showCakeMsgBox : Bool = false
    var strStartTimeSlot = ""
    var strWishCard = ""
    
    class func deliveryProductList(deliveryArray: NSArray) -> NSMutableArray {
        let deliveryDataArray = NSMutableArray()
        for (_, object) in deliveryArray.enumerated() {
            if let deliveryDict = object as? Dictionary<String, AnyObject> {
                let deliveryInfo = FNPDeliveryDetailsInfo()
                
                if let cakeMsg = deliveryDict["cakeMsg"] as? String {
                    deliveryInfo.strCakeMsg = cakeMsg
                }
                if let cardMsg = deliveryDict["cardMsg"] as? String {
                    deliveryInfo.strCardMsg = cardMsg
                }
                if let cartItemIndex = deliveryDict["cartItemIndex"] as? String {
                    deliveryInfo.strCartItemIndex = cartItemIndex
                }
                if let city = deliveryDict["city"] as? String {
                    deliveryInfo.strCity = city
                }
                if let countryGeoId = deliveryDict["countryGeoId"] as? String {
                    deliveryInfo.strCountryGeoId = countryGeoId
                }
                if let countryName = deliveryDict["countryName"] as? String {
                    deliveryInfo.strCountryName = countryName
                }
                if let deliveryDate = deliveryDict["deliveryDate"] as? String {
                    deliveryInfo.strDeliveryDate = deliveryDate
                }
                if let endTimeSlot = deliveryDict["endTimeSlot"] as? String {
                    deliveryInfo.strEndTimeSlot = endTimeSlot
                }
                if let itemSubTotal = deliveryDict["itemSubTotal"] as? String {
                    deliveryInfo.strItemSubTotal = itemSubTotal
                }
                if let occassion = deliveryDict["occasion"] as? String {
                    deliveryInfo.strOccassion = occassion
                }
                deliveryDataArray.add(deliveryInfo)
            }
        }
        return deliveryDataArray
    }
 
}
