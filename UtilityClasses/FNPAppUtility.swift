//
//  FNPAppUtility.swift
//  FNP
//
//  Created by Suresh patel on 22/11/16.
//  Copyright © 2016 Cochify. All rights reserved.
//

import UIKit

let showLog = true

func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

// custom log
func logInfo(_ message: String, file: String = #file, function: String = #function, line: Int = #line, column: Int = #column) {
    if (showLog) {
        print("\(function): \(line): \(message)")
        //Log.writeLog(message: message, clazz: file, function: function, line: line, column: column)
    }
}

var CurrentTimestamp: String {
    return "\(Date().timeIntervalSince1970)"
}

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Get AppFont with Size <<<<<<<<<<<<<<<<<<<<<<<<*/
func kAppFontRegularWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-Regular", size: size)!
}
func kAppFontBoldWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-Bold", size: size)!
}
func kAppFontMediumBoldWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-Black", size: size)!
}
func kAppFontMediumBoldItalicWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-BlackItalic", size: size)!
}
func kAppFontLightWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-Light", size: size)!
}
func kAppFontItalicWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-Italic", size: size)!
}
func kAppFontLightItalicWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-LightItalic", size: size)!
}
func kAppFontBoldItalicWithSize(_ size:CGFloat) -> UIFont {
    return UIFont(name:"Lato-BoldItalic", size: size)!
}
func RGBA(_ r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) -> UIColor {
    return UIColor(red: (r/255.0), green: (g/255.0), blue: (b/255.0), alpha: a)
}

func getImageFromColor(_ color:UIColor) -> UIImage {
    
    let rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size);
    let context: CGContext = UIGraphicsGetCurrentContext()!
    context.setFillColor(color.cgColor);
    context.fill(rect);
    let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext();
    
    return img;
}

func getRatingDescriptionForRating(_ rating:Float) ->NSString {
    // return blank on zero
    if (rating == 1) {
        return "Yuck: Vomited in my mouth a little"
    } else if (rating <= 1.5) {
        return "Awful: Instant regret"
    } else if (rating <= 2) {
        return "Bad: Barely edible"
    } else if (rating <= 2.5) {
        return "Meh: I could make better"
    } else if (rating <= 3) {
        return "Average: Nothing special"
    } else if (rating <= 3.5) {
        return "Good: I would recommend this"
    } else if (rating <= 4) {
        return "Very Good: Drool worthy"
    } else if (rating <= 4.5) {
        return "Outstanding: I'd come here just for this dish"
    } else if (rating <= 5) {
        return "Perfection: What dreams are made of"
    } else {
        return ""
    }
}

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Get WeekDay for date <<<<<<<<<<<<<<<<<<<<<<<<*/
//EEEE for weekday, //"dd/MM/yy", //"yyyy-MM-dd"
//"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSSZ" -->UTC date formate

func getStringFromDate(_ date:Date, format:NSString) -> NSString {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format as String
    
    return dateFormatter.string(from: date) as NSString
}

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Get UnderLined Attributed String <<<<<<<<<<<<<<<<<<<<<<<<*/
func getUnderLinedAttributedString(_ str:NSString) -> NSAttributedString {
    
    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
    let underlineAttributedString = NSAttributedString(string: str as String, attributes: underlineAttribute)
    
    return underlineAttributedString
}
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Get Attributed String <<<<<<<<<<<<<<<<<<<<<<<<*/
//func getAttributedString(_ fullStr:NSString, attributableStr:NSString, color:UIColor, font:UIFont) -> NSAttributedString {
//    //let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
//    //let underlineAttributedString = NSAttributedString(string: fullStr as String, attributes: underlineAttribute)
//    
//    //let effectedStr1:NSString = "(\(attributableStr))"//"("+attributableStr+")"
//    //let mutableAttributedString = NSMutableAttributedString(string: fullStr as String)
//    
//    //let regex = NSRegularExpression.re
//    
//    //myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.greenColor(), range: NSRange(location:10,length:5))
//    
//    //return underlineAttributedString
//    
//    let attributes = [
//        NSFontAttributeName : UIFont(name: "Helvetica Neue", size: 12.0)!,
//        NSUnderlineStyleAttributeName : 1,
//        NSForegroundColorAttributeName : UIColor.red,
//        NSTextEffectAttributeName : NSTextEffectLetterpressStyle,
//        NSStrokeWidthAttributeName : 3.0
//        ] as [String : Any]
//    
//    let atriString = NSAttributedString(string: "My Attributed String", attributes: attributes)
//    
//    return atriString
//}




/*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Setting corner for UIButton <<<<<<<<<<<<<<<<<<<<<<<<*/
func setCornerForButton(_ button:UIButton, cornerRadius:CGFloat, borderWidth:CGFloat, borderColor:UIColor) {
    button.layer.cornerRadius =  cornerRadius
    button.layer.borderColor = borderColor.cgColor
    button.layer.borderWidth = borderWidth
    button.clipsToBounds = true
}

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Setting corner for UIButton <<<<<<<<<<<<<<<<<<<<<<<<*/
func setCornerForImageView(_ imageView:UIImageView, cornerRadius:CGFloat, borderWidth:CGFloat, borderColor:UIColor) {
    imageView.layer.cornerRadius =  cornerRadius
    imageView.layer.borderColor = borderColor.cgColor
    imageView.layer.borderWidth = borderWidth
    imageView.clipsToBounds = true
}

// convert images into base64 and keep them into string
func convertImageToBase64(_ image: UIImage, compressionQuality:CGFloat) -> String {
    
    let imageData = UIImageJPEGRepresentation(image, compressionQuality)
    let base64String = imageData!.base64EncodedString(options: [])
    
    return base64String
    
}// end convertImageToBase64

func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
    if let data = text.data(using: String.Encoding.utf8) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
            return json
        } catch {
            //print("Something went wrong    \(text)")
        }
    }
    return nil
}

func trimWhiteSpace (_ str: String) -> String {
    let trimmedString = str.trimmingCharacters(in: CharacterSet.whitespaces)
    return trimmedString
}

func giveLeftPaddingToTextField(_ textField: UITextField, width: Int , height: Int) {
    
    let leftPaddingView = UIView(frame: CGRect(x: 0,y: 0, width: width, height: height))
    textField.leftView = leftPaddingView
    textField.leftViewMode = .always
}

func jwtTokenInfo(_ tokenStr:String) -> Dictionary<String, AnyObject>? {
    
    let segments = tokenStr.components(separatedBy: ".")
    
    var base64String = segments[1] as String
    
    if base64String.characters.count % 4 != 0 {
        let padlen = 4 - base64String.characters.count % 4
        base64String += String(repeating: "=", count: padlen)
    }
    
    if let data = Data(base64Encoded: base64String, options: []) {
        do {
            let tokenInfo = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            return tokenInfo as? Dictionary<String, AnyObject>
        } catch {
            logInfo("error to generate jwtTokenInfo >>>>>>  \(error)")
        }
    }
    return nil
}


class FNPAppUtility: NSObject {

    
    class func saveSessionTokenInDefault(sessionToken : String){
        UserDefaults.standard.setValue(sessionToken, forKey: kHeaderToken)
        UserDefaults.standard.synchronize()
    }
    
    class func getSessionTokenFromDefault() ->String{
        let savedToken = UserDefaults.standard.value(forKey: kHeaderToken)
        if savedToken == nil {
            return ""
        }
        return savedToken as! String
    }
    
    class func deleteSessionToken(){
        UserDefaults.standard.removeObject(forKey: kHeaderToken)
        UserDefaults.standard.synchronize()
    }
    
    
    class func saveFnpTokenInDefault(fnpTokenId : String) {
        UserDefaults.standard.setValue(fnpTokenId, forKey: kfnpAccessToken)
        UserDefaults.standard.synchronize()
    }
    
    class func deleteFNPToken(){
        UserDefaults.standard.removeObject(forKey: kfnpAccessToken)
        UserDefaults.standard.synchronize()
    }
    
    class func getFnpTokenFromDefault() ->String {
        let fnpToken = UserDefaults.standard.value(forKey: kfnpAccessToken)
        if fnpToken == nil {
            return ""
        }
        return fnpToken as! String
    }
    

    class func saveCustomerIdInDefault(customerId : String) {
        UserDefaults.standard.setValue(customerId, forKey: KCustomerId)
        UserDefaults.standard.synchronize()
    }
    
    class func getCustomerIdFromDefault() ->String {
        let customerId = UserDefaults.standard.value(forKey: KCustomerId)
        if customerId == nil {
            return ""
        }
        return customerId as! String
    }
    
    class func deleteCustomerId(){
        UserDefaults.standard.removeObject(forKey: KCustomerId)
        UserDefaults.standard.synchronize()
    }
    
    class func saveCustomerNameInDefault(customerName : String) {
        UserDefaults.standard.setValue(customerName, forKey: kUSERNAME)
        UserDefaults.standard.synchronize()
    }
    
    class func getCustomerNameFromDefault() ->String {
        let customerName = UserDefaults.standard.value(forKey: kUSERNAME)
        if customerName == nil {
            return ""
        }
        return customerName as! String
    }
    
    class func deleteCustomerName(){
        UserDefaults.standard.removeObject(forKey: kUSERNAME)
        UserDefaults.standard.synchronize()
    }
    
    class func saveCustomerEmailInDefault(customerEmail : String) {
        UserDefaults.standard.setValue(customerEmail, forKey: Kemail)
        UserDefaults.standard.synchronize()
    }
    
    class func getCustomerEmailFromDefault() ->String {
        let customerEmail = UserDefaults.standard.value(forKey: Kemail)
        if customerEmail == nil {
            return ""
        }
        return customerEmail as! String
    }
    
    class func deleteCustomerEmail(){
        UserDefaults.standard.removeObject(forKey: Kemail)
        UserDefaults.standard.synchronize()
    }
}
