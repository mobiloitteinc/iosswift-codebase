//
//  FNPSearchVC.swift
//  Fernsnpetals
//
//  Created by Yogita Joshi on 28/11/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//
//FNPSearchCell
import UIKit

protocol SearchDelegate {
    func searchCallbackMethodWith(title:String, category:String, searchQS:String)
}

protocol productSearchDelegate {
    func productSearchCallbackMethodWith(title:String, category:String, searchQS:String)
}

class FNPSearchVC: UIViewController , UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate{
    
    @IBOutlet var txtSearch             : UITextField!
    @IBOutlet var backBtn               : UIButton!
    @IBOutlet var navView               : UIView!
    @IBOutlet var tableView             : UITableView!
    @IBOutlet weak var cancelButton     : UIButton!
    var searchDataSourceArray           = NSMutableArray()
    var searchArray                     = NSMutableArray()
    var isResultFound                   : Bool! = false
    var delegate: SearchDelegate?
    var delegateSearchproduct           : productSearchDelegate?
    var isFromProductDetailVC           : Bool! = false
    
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUpMethod()
        print("",delegate ?? "")
    }
    
    //MARK:- Initial SetUp Method
    private func initialSetUpMethod() {
        self.txtSearch.delegate = self
        self.txtSearch.becomeFirstResponder()
        let str = NSAttributedString(string: "Search flowers, gifts, chocolates...", attributes: [NSForegroundColorAttributeName:RGBA(255, g: 255, b: 255, a: 0.8)])
        self.txtSearch.attributedPlaceholder = str
        
        //register tableViewCell
        let nibName = UINib(nibName: "FNPSearchCell", bundle:nil)
        tableView.register(nibName, forCellReuseIdentifier: "FNPSearchCell")
    }
    
    //MARK: - UITableView Datasource & Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isResultFound == true {
            return searchDataSourceArray.count
        }
        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FNPSearchCell", for: indexPath) as! FNPSearchCell
        cell.selectionStyle = .none;
        if isResultFound == true {
            let objSearchData = searchDataSourceArray[indexPath.row]
            cell.lblItemName!.text = (objSearchData as! FNPSearchInfo).labelString
        }else {
            let objSearchData = searchArray[indexPath.row]
            cell.lblItemName!.text = (objSearchData as! FNPSearchInfo).labelString
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.txtSearch.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: true)
        
        var objSearchData = FNPSearchInfo()
        
        if isResultFound == true {
            objSearchData = searchDataSourceArray[indexPath.row] as! FNPSearchInfo
        }else {
            objSearchData = searchArray[indexPath.row] as! FNPSearchInfo
        }
        
        if (self.delegate != nil) {
            let _ = navigationController?.popViewController(animated: false)

            self.delegate?.searchCallbackMethodWith(title: (String(format: "Product matched %@", (objSearchData ).categoryString) as NSString) as String, category: (objSearchData ).categoryString, searchQS:"")
            
        }
        else if  (self.delegateSearchproduct != nil) {
            let _ = navigationController?.popViewController(animated: false)
            
            self.delegateSearchproduct?.productSearchCallbackMethodWith(title: (String(format: "Product matched %@", (objSearchData ).categoryString) as NSString) as String, category: (objSearchData ).categoryString, searchQS:"")
        }
        else if isFromProductDetailVC == true {
            
            var productListVC: FNPProductListVC?
            for objVC in (navigationController?.viewControllers)! {
                if let objVC = objVC as? FNPProductListVC {
                    // found
                    productListVC = objVC
                    break
                }
            }
            productListVC?.productDetailMethod(title:(String(format: "Product matched %@", (objSearchData ).categoryString) as NSString) as String, category: (objSearchData ).categoryString, searchQS:"")
            
            guard let controller = productListVC else {
                // controller seems not found or nil
                
                return
            }
            
            let _ = navigationController?.popToViewController(controller, animated: true)
        }
        else {
            let newViewController = FNPProductListVC(nibName: "FNPProductListVC", bundle: nil)
            newViewController.categoryID = (objSearchData ).categoryString
            newViewController.strTitle = String(format: "Product matched %@", (objSearchData ).labelString) as NSString
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    
    // MARK: - UITextField delegate Methods
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if (self.delegate != nil) {
            let _ = navigationController?.popViewController(animated: false)

            self.delegate?.searchCallbackMethodWith(title: "", category: "", searchQS:textField.text!)
            
        }
        else if  (self.delegateSearchproduct != nil) {
            let _ = navigationController?.popViewController(animated: false)

            self.delegateSearchproduct?.productSearchCallbackMethodWith(title: "", category: "", searchQS:textField.text!)
        }
        else if isFromProductDetailVC == true {
            
            var productListVC: FNPProductListVC?
            for objVC in (navigationController?.viewControllers)! {
                if let objVC = objVC as? FNPProductListVC {
                    // found
                    productListVC = objVC
                    break
                }
            }
            productListVC?.productDetailMethod(title:"", category: "", searchQS:textField.text!)
            
            guard let controller = productListVC else {
                // controller seems not found or nil
                
                return false
            }
            
            let _ = navigationController?.popToViewController(controller, animated: true)
        }
        else {
            let newViewController = FNPProductListVC(nibName: "FNPProductListVC", bundle: nil)
            newViewController.qsString = textField.text!
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
   
        return true
    }
    
    // MARK: - IBAction Methods
    @IBAction func backButtonAction(_ sender: Any) {
        let _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.txtSearch.text = ""
        self.cancelButton.isHidden = true
        self.webAPIMethodToSearchTextSuggestion(searchText: self.txtSearch.text!)
    }
    
    @IBAction func textEditing(_ sender: Any) {
        let str:String = self.txtSearch.text!
        if str.length > 0 {
            if self.cancelButton.isHidden {
                self.cancelButton.isHidden = false
            }
        }else {
            self.cancelButton.isHidden = true
            self.searchArray.removeAllObjects()
             self.searchDataSourceArray.removeAllObjects()
             self.tableView.reloadData()
        }
        self.isResultFound = false
        self.webAPIMethodToSearchTextSuggestion(searchText: str)
    }
    
    //MARK:- API Method
    func webAPIMethodToSearchTextSuggestion(searchText : String){
        let paramDict = NSMutableDictionary()
        paramDict[KcatalogId] = KcategoryName
        paramDict["qs"] = searchText
        
        ServiceHelper.callAPIWithParameters(paramDict, method: .get, apiName: KWeb_API_searchTextSuggesions, hudType:.default) { (result, error) in
            if let error = error {
                // show error
                let _ = AlertViewController.alert("Error!", message: error.localizedDescription)
            }
            else {
                // handle response
                if let dataDict = result as? Dictionary<String, AnyObject> {
                    if ((dataDict[Kstatus] as! Int) == 200){
                        self.searchDataSourceArray.removeAllObjects()
                        
                        let dataInfoDict = dataDict["data"] as? Dictionary<String, AnyObject>
                        let categoryDict = dataInfoDict?["categories"] as? Dictionary<String, AnyObject>
                        if let occasionList = categoryDict?["occasion"] as? Array<Dictionary<String, AnyObject>> {
                            var totalSearchArray = occasionList
                            if let productList = categoryDict?["productType"] as? Array<Dictionary<String, AnyObject>> {
                                totalSearchArray = Array([totalSearchArray, productList].joined())
                            }
                            if let recipientList = categoryDict?["recipient"] as? Array<Dictionary<String, AnyObject>> {
                                totalSearchArray = Array([totalSearchArray, recipientList].joined())
                            }
                            self.searchDataSourceArray = FNPSearchInfo.searchList(dataArray: totalSearchArray as NSArray)
                            self.isResultFound = true
                            self.searchArray.removeAllObjects()
                            self.searchArray = self.searchDataSourceArray.mutableCopy() as! NSMutableArray
                        }
                        
                        self.tableView.reloadData()
                    }
                    else {
                        let _ = AlertViewController.alert("Error!", message: "Something went wrong. Please try again.")
                    }
                }
            }
        }
    }
    
    //MARK:- Memory management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
