//
//  FNPRecipientDetail.swift
//  Fernsnpetals
//
//  Created by Yogita Joshi on 12/12/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPRecipientDetail: NSObject {
    
    var recipientNameStr = ""
    var pincodeStr = ""
    var recipientAddressStr = ""
    var cityStr = ""
    var countryCode = ""
    var phoneNumberStr = ""
    var recipientEmailIdStr = ""
    var occasionStr = ""
    var messageStr = ""
    var messageCountStr = ""
    var dateStr = ""
    var placeholderDataArray = [String]()
    
    class func getrecipientDetail(dataDict: [String : AnyObject]) -> FNPRecipientDetail {
        
        let recipientInfo = FNPRecipientDetail()
        
        recipientInfo.recipientNameStr = "\(dataDict.validatedValue(krecipientNameStr, expected: "" as AnyObject))"
        recipientInfo.pincodeStr = "\(dataDict.validatedValue(kpincodeStr, expected: "" as AnyObject))"
        recipientInfo.recipientAddressStr = "\(dataDict.validatedValue(krecipientAddressStr, expected: "" as AnyObject))"
        recipientInfo.cityStr = "\(dataDict.validatedValue(kcityStr, expected: "" as AnyObject))"
        recipientInfo.phoneNumberStr = "\(dataDict.validatedValue(kphoneNumberStr, expected: "" as AnyObject))"
        recipientInfo.recipientEmailIdStr = "\(dataDict.validatedValue(krecipientEmailIdStr, expected: "" as AnyObject))"
        recipientInfo.occasionStr = "\(dataDict.validatedValue(koccasionStr, expected: "" as AnyObject))"
        recipientInfo.messageStr = "\(dataDict.validatedValue(kmessageStr, expected: "" as AnyObject))"
        
        return recipientInfo
    }
}
