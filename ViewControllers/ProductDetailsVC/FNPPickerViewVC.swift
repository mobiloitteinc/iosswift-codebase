//
//  FNPPickerViewVC.swift
//  Fernsnpetals
//
//  Created by Deepali Gupta on 23/12/16.
//  Copyright © 2016 Ferns N Petals. All rights reserved.
//

import UIKit

protocol FNPPickerViewDelegate {
    func didSelectPickerViewOptionWith(_ string: String , andSelectedVal: String)
}

class FNPPickerViewVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    @IBOutlet weak var myPicker : UIPickerView!
    var sortByArray             = [FNPSortInfo()]
    var selectedString          = ""
    var delegate                : FNPPickerViewDelegate?
    var selectedValue           = ""
    
    //MARK:- UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        myPicker.dataSource = self
        myPicker.delegate   = self
        myPicker.reloadAllComponents()
    }
    
    //MARK: Data Sources
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return sortByArray.count
    }
    
    //MARK: Delegates
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        let sortLabel = sortByArray[row]
        return sortLabel.sortLevel
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        let sortLabel = sortByArray[row]
        selectedString = sortLabel.sortValue
        selectedValue = sortLabel.sortLevel
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil){
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Roboto-Regular", size: 18)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        let sortLabel = sortByArray[row]
        pickerLabel?.text = sortLabel.sortLevel
        return pickerLabel!;
    }
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 22.0
    }
    
    //MARK:- Memory management Methods
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction Methods
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        if selectedString == ""{
           selectedString = "SEQUENCE_NUM_gifts|ASC"
            if sortByArray.count>0 {
                let sortInf = sortByArray[0] as FNPSortInfo
                self.selectedValue = sortInf.sortLevel
            }
         }
        if self.delegate != nil {
            print(selectedString as String)
            print(self.selectedValue)
            self.delegate?.didSelectPickerViewOptionWith(selectedString as String, andSelectedVal: self.selectedValue)
        }
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
