//
//  FNPServiceHandler.swift
//  Fernsnpetals
//
//  Created by Navneet Sharma on 22/03/17.
//  Copyright © 2017 Ferns N Petals. All rights reserved.
//

import UIKit

class FNPServiceHandler: NSObject {

    func execTask(request: URLRequest, secondRqst:URLRequest, taskCallback: @escaping (
        URLResponse?, Data, URLRequest) -> ()) {
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            if let data = data {
                
               
                // logInfo("\n\n Response Code >>>>>> \n\(responseCode) \nResponse String>>>> \n \(responseString)")
               
                
                taskCallback(response, data, secondRqst)
                
//                let json = try? JSONSerialization.jsonObject(with: data, options: [])
//                if let response = response as? HTTPURLResponse , 200...600 ~= response.statusCode {
//                    taskCallback(json as AnyObject?, data, secondRqst)
//                } else {
//                    taskCallback(json as AnyObject?, data, secondRqst)
//                }
            }
        })
        task.resume()
    }
}
